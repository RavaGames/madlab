# Examples

Listed are several functionality XML examples, with detailed explanations of the attributes.

[[_TOC_]]

## General Information:

MaD2 functionality XML is in the format `<node attribute="value" />`. Anything else won't work.

Attributes are added into the XML as `attribute="value"`. All values, even numbers, must be inside quotation marks.

MaD2 will automatically add `label="Item Name In MaD Lab"` and `object="itemnameinmadlab"` attributes to the XML, so adding them manually is not necessary.

The below examples are broken into multiple lines for legibility. Make sure the attributes are spaced properly if you edit XML as multiline in a text editor, as Lab will remove newlines when pasting in the text.


## Trigger Examples

### Grosspfeil

Grosspfeil is a very heavy arrow that violently cuts anything that even slightly touches its blade.

The arrow is available as its own item in the Library, but it is also used as a projectile by Grossbogen (see [Firearm Examples](#firearm-examples).)

![](../img/grossbow.png)

```xml
<node 
trigger="triggers::CutOnCollision" 
triggered="true" 
triggerForce="50" 
triggerColour="333333" 
weights="10,1,1" 
tooltip="Very, very, sharp." 
lab="true" 
/>
```

`trigger` is always in the format `triggers::TriggerName` for boring technical reasons.
See Triggers file for a list of available triggers.

`triggered=true` makes the item be triggered instantly after spawning, making its functionality actually work.

`triggerForce` is used for different purposes in different triggers, usually to signify magnitude. In this case, it's the length of the cut on collision.

`triggerColour` is used in trigger functionalities to detect specific layer collisions. In this case, _CutOnCollision_ triggers when the tip of the arrow (collider layer that is coloured #333333) touches something.

`weights` modifies the mass of the collider layers. In this case the arrow's tip is very heavy.

`tooltip` adds a functionality tooltip to the item in Library and Spawn menus. Add `&#xA;` for newline in longer tooltips. The amount of newlines is used to scale the tooltip box, so add more to the end if the text overflows the box.

### Camera Gimbal

Camera Gimbal pulls linked items on top of it and smoothens their rotation.

```xml
<node 
trigger="triggers::CameraGimbal" 
triggerForce="100" 
triggerRadius="100" 
triggerPos="0,-50" 
triggered="true" 
lab="true" 
links="true" 
tooltip="Holds linked items and smoothens their rotation.&#xA;" 
/>
```

`triggerForce` is used to magnify the pull force.

`triggerPos` is used to set the default position above the gimbal.

`triggerRadius` is used to multiply the pull position, so players can shift it even higher in case they want to hold a larger item.

`links=true` adds a note about the item functionality relying on links in Spawn menu.

![](../img/cameragimbal.png)

## Armour Examples 

### Footwear

Feet for your feet.

![](../img/footwear.png)

```xml
<node 
armour="boot" 
x="-8" 
y="15" 
health="100" 
blunt="0.2" 
bullet="1.0" 
cut="100" 
angle="0" 
cutProtect="false" 
allowOther="true" 
flipX="true" 
lab="true" 
tooltip="Bad at standing." 
/>
```

`armour="boot"` turns the item into an armour, and makes it automatically equip on feet.

`health` predictably sets the armour's maximum health. When an armour runs out of health, it will break.

`blunt` / `bullet` damage modifiers. 0.5 deals half damage.

`cut` damage the item receivers per cut per distance (longer cuts deal more damage.) At 100 health and 100 cut damage, the armour will basically instantly disintegrate when cut, because skin isn't the most protective of materials.

`angle=0` equip angle offset in degrees, in case it makes more sense for the item to spawn upright, but equip sideways (e.g. arms.)

`x` / `y` sets the equip position. Note that these values will be applied to `triggerPos`, and may interfere with item or firearm functionalities.

`flipX=true` makes the equip position flip when the armour piece is flipped. Because this item is not symmetrical, flipping the position makes it equip properly on the other foot.

`cutProtect=false` this is not a protective item, so cuts will pass through it to the bodypart.

`allowOther="true"` other armour pieces can be equipped on top of the feet.

### Suppressor Head

[See below.](#suppressor-head-1)

## Firearm Examples

See [Firearm](./Attributes/Firearm.md) for details.

### Grossbogen

A very large bow that shoots a very large arrow.

![](../img/grossbow.png)

```xml
<node 
projectile="grosspfeil" 
ignore="grosspfeil" 
muzzleX="30" 
muzzleFlipX="-30" 
force="500" 
reloadTime="60" 
sounds="whump" 
volumes="1" 
tooltip="Shoots very sharp and very large arrows.&#xA;&#xA;" 
lab="true" 
/>
```

`projectile="grosspfeil"` the bow uses the unique name of an item, making that item spawn with all of its own functionality. Any unique item name can be used instead of using a projectile from the [Projectiles](./Data/Projectiles.md) list.

`ignore="grosspfeil"` since the bow spawns the projectile overlapping itself, it is ignored so the two won't collide.

`muzzleX=30` set the horizontal position where the projectile spawns, slightly towards the front.

`muzzleFlipX` is not set, making it defaulting to -30, negative of `muzzleX`. This bow is horizontally symmetrical enough for it not to matter, and it ignores its projectile so it can't shoot itself even if the shooting position is offset when the item is flipped.

`muzzleY` is not set, making it default to 0. The item is vertically symmetrical so spawning the projectile in the centre is right.

`force=500` to make the arrow shoot out quite fast indeed.  
This is the velocity applied to the projectile, ranging from around:  
* \<150 for slow speed projectiles (flame, plungers, flares, etc)  
* \>150 for launched projectiles (arrows, potatoes, and the like.),  
* \>250 for basic projectiles (smgs/pistols),  
* \>500 for powerful projectiles (rifles), and  
* \>1000 for ludicrous projectiles (snipers)

`reloadTime=60` to make the bow shoot once a second, at 60fps.

`sounds` / `volumes` to set the firing sound to a juicy whump. Both are required for the sound to work.

### Suppressor Head

It's a gun that's also an armour that's also a head. Fancy.

An example of combining firearm and armour attributes.

![](../img/suppressorhead.png)

```xml
<node 
armour="head" 
x="0" 
y="3" 
health="1000" 
blunt="1.0" 
bullet="1.0" 
cut="50" 
angle="0" 
cutProtect="false" 
hideHead="true" 
allowOther="true" 
collideDolls="false" 
flipX="false" 
flipY="false" 
properties="alive" 
lab="true" 
projectile="pis" 
muzzleX="50" 
muzzleY="-3" 
triggered="false" 
magazineSize="12" 
cycleTime="30" 
reloadTime="60" 
sounds="cut1,cut2" 
volumes="1,1" 
tooltip="Pew pew. Splurt splurt." 
/>
```

`hideHead="true"` hides the doll's head when the armour piece is equipped. Can be used for head replacements (e.g. Caved Head)

`allowOther="true"` because the item looks like a head, it makes sense to allow other armour pieces to be combined with it.

`properties="alive"` Semi-supported override that replaces armour bullet holes with bloody ones, and applies alive blunt sounds. Collision functionality is still handled by the armour, so cuts and blunt damage won't bleed.

----

## Console Commands  
_Usage_: Open the console by pressing the `[Home]` key. Type a command. Press Enter. Close by unfocusing and pressing the key again, or by entering `close`.

* `id <Item Library Name>` (without "<>") to retrieve the unique name of an item (used for `ignore="uniquename"` in general, and `projectile="uniquename"` to make firearms shoot out items.)

* `xml <Item Library Name>` (without "<>") to retrieve the XML of existing items.  
  Note that most old items don't use the XML functionality format, so the command will only return basic information and nothing related to functionality. _Cryolab_ and _Hattoween 2_ updates were made using XML functionality.
