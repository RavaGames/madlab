# Kong User Content Archive

As web Flash End-of-Life is now in effect, the Kong user content was archived by the [MaD2 Discord](https://discord.com/invite/mad2) to ensure it can still be used by those who wish to do so.

The archive is composed of content retrieved as-is from Kong in Jan 2021. Authors have personally chosen to share the content publicly in whichever state it is in.

Each of the content files contains all of the publicly-shared content that was available at the time the archive was made.

* 4010   [**Contracts**   ](./contract.txt)   (10MB)  
* 45291  [**Roleplays**   ](./roleplay.txt)   (190MB)  
* 54147  [**Levels**      ](./level.txt)      (170MB)  
* 63890  [**Custom Items**](./customitem.txt) (120MB)


Private content is unfortunately not available after the EoL, as Kong doesn't offer a way to access it through profiles directly and the game doesn't work on the web any more. You may be able to find it in your [local content files](https://steamcommunity.com/app/665370/discussions/0/1795152172925405479/#c1795152172925652233).

# Usage

## Manually

1) Download the [content files](https://gitlab.com/RavaGames/madlab/-/tree/master/Content) you're interested in. They're are quite massive, with all the public content ever shared, so they can't be used directly online.

2) Use your favourite text editor to open the file. It may take some time to open depending on the software.

3) Search for things:  
  If you're looking for a specific piece of content, try searching for it by using its name or any keywords you remember.  
  If you're looking for your own content, search for '_"author":"YourKongUserName"_'

4) Copy the save string:  
  The data contains the strings in the format '_"content":"SaveStringHere"_'.  
  Save strings may start with _'MaD'_ or _'eN'_, and they may end in anything.  
  Make sure to copy it entirely or it won't work. Don't include quotes.

5) Paste the copied save string in-game.

6) Enjoy.

## Programmatically

1) Download the category file(s) you're interested in.

2) Use your favourite method of parsing text files and JSON to find, browse, sort, or generally mess with the data.

3) Enjoy.

**Details**:

- The files contain one piece of content per line.

- The content format is JSON, as Kong provided it.

- The data contains quite a bit of Kong-specific things that are now quite irrelevant after the EoL.  
Here are the relevant ones: 

````
{
    "content" : "The MaD2 save string.",
    "author"  : "Kong username of the author.",
    "name"    : "Name of the content, as filled by its author.",
    "desc"    : "Description of the content, as filled by its author.",
    "rating"  : "0.0 to 5.0, with 0.0 being unrated with not enough ratings.",
    "loads"   : "Number of times the content was loaded on Kong."
}
````



