# Firearm Attributes
Turn a boring model into a beautiful firearm.

All attributes are added into the XML as `attribute="value"`. All values, even numbers, must be inside quotation marks.

_Example_: Suppressor Head  
```xml
<!--Armour portion of the XML omitted.-->
<node
projectile="pis"
muzzleX="50"
muzzleY="-3"
muzzleFlipX="-50"
triggered="false"
magazineSize="12"
cycleTime="30"
reloadTime="60"
sounds="cut1,cut2"
volumes="1,1"
tooltip="Pew pew. Splurt splurt."
/>
```

----
## Firearm Attributes

* **projectile**  
  Required to turns the item into a firearm. See [Projectiles](https://gitlab.com/RavaGames/madlab/-/blob/master/Data/Projectiles.md) file for available types.  
  _Usage_: Projectile Name
  _Example_: `projectile="rif"` to make a rifle. 

* **size**  
  Bullet model size multiplier.  
  _Usage_: Positive Number  
  _Example_: `size="2"` to make a massive bullet 

* **amount**  
  How many projectiles in a single shot?  
  _Usage_: Positive Integer  
  _Example_: `amount="8"` to make a shotgun. 

* **force**  
  Muzzle velocity. Note that very high values are capped by the physics engine, capping at around 3000.  
  _Usage_: Number  
  _Example_: `force="5"` to make the bullet essentially fall out of the gun.  
  _Example_: `force="300"` to shoot a normal bullet at the same velocity Assault Rifle (AK) uses.  

* **recoil**  
  Backwards recoil force applied to the gun.  
  _Usage_: Number  
  _Example_: `recoil="20"` to recoil as much as Assault Rifle (AK) does.
  _Example_: `recoil="-100"` to make the gun fly in the direction it shoots.  

* **scatter**  
  Inaccuracy force applied to the projectile(s). Higher value makes the firearm less accurate.  
  _Usage_: Positive Number  
  _Example_: `scatter="50"` to spread as much as Shotgun does.  
  _Example_: `scatter="350"` to spread as much as Firespread does.  

* **muzzleX**  
  Horizontal position where the bullet spawns when the firearm is not flipped.  
  _Usage_: Number. Negative is towards the left of model centre, positive is towards the right. Experiment to get the feel for the positions.  
  _Example_: `muzzleX="-50"` to spawn the projectile -50 pixels to the left.  

* **muzzleFlipX**  
  Horizontal position where the bullet spawns when the firearm is flipped. Used to account for flipping centre offset.  
  _Usage_: -muzzleX+offset  
  _Example_: `muzzleX="50"` to spawn the projectile 50 pixels to the right.  

* **muzzleY**  
  Vertical position where the bullet spawns.  
  _Usage_: Number. Negative is up from the model centre, positive is down.  
  _Example_: `muzzleY="-50"` to spawn the projectile upwards by quite a bit (e.g. Wheeled Cannon) 

* **reload**  
  Time between reloads.  
  _Usage_: Positive Number (in frames)  
  _Example_: `reload="60"` shoots once every second (at 60fps.)  

* **magazineSize**  
  How many projectiles are shot before reload is required.  
  _Usage_: Comma-separated property names.  
  _Example_: `=""` 

* **cycleTime**  
  If magazineSize is larger than 1, this value is used between individual shots.  
  _Usage_: Positive Number (in frames)  
  _Example_: `cycleTime="30"` shoots twice every second (at 60fps.)  

* **burst**  
  How many bullets are shot in a burst.  
  _Usage_: Positive Integer  
  _Example_: `burst="3"` e.g. Battle Rifle  

* **barrelAttachments**  
  Disables attaching barrel attachments to the firearm. Default is true, doesn't need to exist in the XML unless setting to false.   
  _Usage_: false  
  _Example_: `barrelAttachments="false"` used for some weapons that don't support barrel attachments thematically, or weirder weapons e.g. Devolver which has a wonky barrel that wouldn't work with attachments.  

* **continuousTrigger**  
  Disable firearm untriggering after each shot. Default is true, doesn't need to exist in the XML unless setting to false.  
  _Usage_: false  
  _Example_: `continousTrigger="false"` used for things like Turret, which is technically a firearm, but runs its own logic to decide when the shoot. Otherwise it would shoot once and turn off.  

* **bulletProperties**  
  Properties that are only added to the projectile.  
  _Usage_: Comma-separated property names. See Properties file for available values.  
  _Example_: `bulletProperties="poisoning"` turns the bullets poisonous, without causing the firearm to poison its wielder.  

* **sounds**  
  List of sounds the firearm uses randomly when shooting.  
  _Usage_: Comma-separated sound names. REQUIRES matching-length volumes list. See Sounds file for available values.  
  _Example_: `sounds="cut1,cut2"` to make the gun make blood splurt sounds like Suppressor Head.  

* **volumes**
  List of sound volumes.  
  _Usage_: Comma-separated numbers. 1 = 100% = default volume.  REQUIRES matching-length sounds list.  
  _Example_: `volumes="2,1"` to make cut1 louder than cut2 in the above example.  




  _
