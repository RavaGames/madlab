# General Attributes

These attributes can be applied in addition to triggers/firearm/armour attributes.

All attributes are added into the XML as `attribute="value"`. All values, even numbers, must be inside quotation marks.

[[_TOC_]]

    
----

## _Example_: Grosspfeil  
```xml
<node
trigger="triggers::CutOnCollision"
triggerForce="50"
triggered="true"
triggerColour="333333"
weights="10,1,1"
lab="true"
tooltip="Very, very, sharp."
/>
```

----
## Spawn Attributes
Setting spawn attributes will override default spawn settings, but not those set by the player in the Spawn Settings menu.
E.g. If the player has Weight set to 50%, and you have the model set to 200%, it will spawn at 50% weight.

* **friction**  
  Modifies physics friction.  
  Higher values make the item slide around less.  
  Default is 50%.  
  _Usage_: Positive Percentage (without %-sign)  
  _Example_: `friction="0"` makes the item slide around forever.

* **weight**  
  Modifies physics mass.  
  Higher values make the item heavier. 0% weight turns items static.
  Default is 100%.  
  _Usage_: Positive Percentage  
  _Example:_ `weight=200` makes the item twice as heavy.  
  See also: `weights` under model attributes.

* **bounce**  
  Modifies physics restitution.  
  Higher values make the item bounce more. Values above 100% will make the item gain speed as it bounces.  
  Default is 20%.  
  _Usage_: Positive Percentage  
  _Example:_ `bounce=100` makes the item bounce endlessly.

* **dampSpeed**  
  Modifies physics linear velocity dampening (aka movement speed.)  
  Default is 0.
  Higher values make the item slow down faster.  
  Note that the effect also affects fall speed, making gravity feel floaty.
  _Usage_: Positive Flat Value  
  _Example:_ `dampSpeed=1000` makes the item float midair, slowly sinking thanks to gravity.

* **dampSpin**  
  Modifies physics angular velocity dampening (aka spinning speed.)  
  Default is 0.  
  Higher values make the item slow down faster.  
  _Usage_: Positive Flat Value  
  _Example:_ `dampSpin=1000` makes the item only spin when pushed or pulled quite hard indeed.

----
## Item Attributes

* **properties**  
  Which properties affect the item?  
  _Usage_: Comma-separated property names. See [Properties](https://gitlab.com/RavaGames/madlab/-/blob/master/Data/Properties.md) file for available values.  
  _Example_: `properties="sticky,radioactive"` to make a dirty, dirty, item.

* **triggered**  
Is the item's functionality active?  
_Usage_: true/false  
_Example_: `triggered="true"` to spawn the item triggered.

* **ignore**  
  The item will not collide with any item with the ignored name.  
  _Usage_: One model name  
  You can find model names using the console command `whatis <Item's Name>`.
  This is necessary because some model names don't correspond 1:1 to their library names, e.g. `whatis Beenade` => `beegrenade`  
  _Example_: `ignore="grosspfeil"` Grossbogen bow does not collide with its arrows, so they can be spawned overlapping each other.  

----
## Model Attributes

* **weights**  
  Modifies collider weights of the model.  
  _Usage_:  
  A) Number. All colliders will be set to this weight.  
  _Example_: weights="2" sets all the colliders to be twice as heavy as default (same as setting weight to 200% in spawn settings.)  
  or  
  B) Comma-separated numbers. Colliders will be set to the listed values, in the layer order.  
  _Example_: weights="10,1,1" sets the first collider to be much heavier than the other two.

* **lab**  
  _Usage_: true  
  Adds creator credit badge to the item in the Spawn menu. The creator credit is automatically copied from the model file.
  _Example_: lab="true" makes the credit appear in Spawn menu.  














  
