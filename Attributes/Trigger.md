# Trigger Attributes

These attributes can be applied in addition to general attributes.  
[Firearm](../Attributes/Firearm.md) and [Armour](../Attributes/Armour.md) attributes may work when combined with Trigger Attributes, but may also result in issues (e.g. trigger position is also used by armour positioning.)

All attributes are added into the XML as `attribute="value"`. All values, even numbers, must be inside quotation marks.

These attributes vary wildly between different trigger functionalities, which are not even feasible to document extensively since there's a thousand of them.  
Please contact rava via [MaD2 Discord](../CONTRIBUTING.md), or experiment yourself to figure out what triggers use what attributes.

    
----

## _Example_: Camera Gimbal  

```xml
<node
trigger="triggers::CameraGimbal"
triggerForce="100"
triggerRadius="100"
triggerPos="0,-50"
triggered="true"
lab="true"
links="true"
tooltip="Holds linked items and smoothens their rotation.&#xA;"
/>
```

----

* **trigger**  
  Adds a trigger functionality to the item. Some triggers require other attributes, if it doesn't work with just the name, add everything else listed below and try again.  
  _Usage_: `trigger=triggers::TriggerName` See [Triggers](../Data/Triggers.md) for available trigger names.  
  _Example_: `trigger="triggers::Pocket"` adds Wallet's functionality to the item (contains one money, and once it's removed, can contain other items.) No other attributes are required for the functionality to work.
  
* **triggerForce**  
  Does different things based on the trigger's functionality.  
  Generally used to scale the effectiveness of the trigger, e.g. cut length, explosion force, push intensity.
  _Usage_: Number (either a flat value, or a percentage)  
  _Example_: `Attribute="0"`
  
* **negativeForce**  
  Allow negative trigger force to be entered via Detail tool? Most items don't allow negative forces because it would not work, or it would break everything.  
  _Usage_: true (default is false and doesn't need to be added to the XML)  
  _Example_: `negativeForce="true"`
  
* **triggerRadius**  
  Generally used to scale the range or distance of the trigger, e.g. explosion radius, or the area of detection around the item. Does different things based on the trigger's functionality.  
  _Usage_: Positive Number (usually in pixels, sometimes a percentage multiplier instead)  
  _Example_: `triggerRadius="100"`
  
* **triggerDuration**  
  Generally used for duration-based things e.g. countdown before item re-triggers. Does different things based on the trigger's functionality.  
  _Usage_: Positive Number (in frames)   
  _Example_: `triggerDuration="60"` to make something happen every second at 60fps.
  
* **triggerPos**  
  Used to set a position of a trigger functionality, e.g. radius offset on staves. Does different things based on the trigger's functionality.  
  _Usage_: Comma-separated x,y (in pixels)  
  _Example_: `triggerPos="0,-100"` to set the trigger position above the item's centre.
  
* **triggerColour**  
  Does different things based on the trigger's functionality.  
  Generally used to detect specific layer collisions.  
  _Usage_: Six-digit hex string without "#"  
  _Example_: Powerful Drill only cuts when the drill-bit touches something. The collider is marked with a unique colour in MaD Lab, the same as triggerColour.  
  `trigger="triggers::CutOnCollision" triggerForce="10" triggered="true" triggerColour="CCCCCC"`
  
* **triggerCustom**  
  Used when an item needs more attributes than the basic ones can offer.  
  _Usage_: Depends on the item. Most items that use it have a single additional value (e.g. Pocket Ball stores the captured item there), but others may implement custom functionality.  
  _Example_: Both Banana Magazine and Rifle Magazine are made out of the same trigger functionality by changing the custom attribute values.  
  `trigger="triggers::ProjectileChange" triggerCustom="bullet=banana,size=100,fromX=-4,fromY=-25,toX=-4,toY=-28"`
  `trigger="triggers::ProjectileChange" triggerCustom="bullet=rif   ,size=100,fromX=-4,fromY=-18,toX=-4,toY=-24"`
  
* **triggerContact**  
  Triggers the item when enough force is applied.  
  _Usage_: Positive Number  
  _Example_: `triggerContact="10"` to make Head Trap explode on very weak collisions.
    
* **triggerCollide**  
  Force-based on-collision functionality requires this much force to trigger.  
  _Usage_: Positive Number  
  _Example_: `triggerCollide="50"` makes Concuss Baton only apply its stun effect when it's swung hard enough.
  
* **triggerState**  
  Internal state for multi-state items, or item variants. You will most likely not need this.  
  _Usage_: Integer  
  _Example_: Picky Magnet uses the same functionality as Magnet, but `triggerState=1` makes it only apply to linked items.  
  `trigger="triggers::Magnet"	triggerState="1"	negativeForce="true"	triggerForce="100"	triggerRadius="100"`
  
* **triggerOnCut**  
  Triggers when cut.  
  _Usage_: true (default is false and doesn't need to be added to the XML)  
  _Example_: `triggerOnCut="true"` e.g. if you attempt to cut a Head Trap, it will do its thing.
  
* **triggerOnContact**  
  Triggers on any collision.  
  _Usage_: true (default is false and doesn't need to be added to the XML)  
  _Example_: `triggerOnContact="true"` e.g. when a Saw Gun's projectile hits anything, it will be disintegrated after a while to clean up.
  
