# Armour Attributes

These attributes can be applied in addition to general/firearm attributes.  
[Trigger](../Attributes/Trigger.md) attributes may work when combined with Armour Attributes, but may also result in issues (e.g. trigger position used by armour positioning, and may be used by trigger functionality.)

All attributes are added into the XML as `attribute="value"`. All values, even numbers, must be inside quotation marks.
    
----

## _Example_: Footwear  

Feet for your feet.

![](../img/footwear.png)

```xml
<node
armour="boot"
x="-8"
y="15"
health="100"
blunt="0.2"
bullet="1.0"
cut="100"
angle="0"
cutProtect="false"
allowOther="true"
flipX="true"
lab="true"
tooltip="Bad at standing."
/>
```

----

* **armour**  
  Turns the item into an armour, making it attach to the bodypart.  
  _Usage_: helmet, chest, torso, stomach, belt, thigh, boot, shoulder, glove, back (invalid values will attach to the head)
  _Example_: Brassclad Wing attaches to the back.
  `armour="back" x="30" y="6" health="1000" blunt="0.2" bullet="0.5" cut="100" allowOther="true"`  
  
* **health**  
  Sets the armour's maximum health. When an armour runs out of health, it will break.  
  _Usage_: Number Above Zero  
  _Example_: `health=1000` to make a hefty armour.  
  
* **blunt**  
  Blunt damage modifier.  
  _Usage_: Percentage Number (without %)  
  _Example_: `blunt=0.5` to receive half blunt damage.  
  
* **bullet**  
  Bullet damage modifier.  
  _Usage_: Percentage Number (without %)  
  _Example_: `blunt=0.1` to receive minimal bullet damage.
  
* **cut**  
  Flat damage the item receivers per cut per distance (longer cuts deal more damage.)  
  _Usage_: Number  
  _Example_: cut="100" a long cut across an armour piece will heavily damage it, or even instantly break.  
  
* **x** / **y**  
  Sets the equip x/y position offset. Note that these values will be applied to `triggerPos`, and may interfere with item or firearm functionalities.  
  _Usage_: Number (where 0,0 is center, and top left is towards negative xy)  
  _Example_: `x="-50" y="-50"` to make the armour piece equip upper left of a bodypart.  
  
* **flipX**  
  Makes the horizontal equip position flip when the armour piece is flipped.  
  _Usage_: true (default is false and not required.)  
  _Example_: flipX="true" is required on Footwear because it's not symmetrical. Flipping the position makes it equip properly on the other foot.  
  
* **flipY**  
  Makes the vertical equip position flip when the armour piece is flipped.   
  _Usage_: true (default is false and not required.)  
  _Example_: Generally used for gloves and shoulders, which equip sideways, because arms and hands also spawn sideways.  
  E.g. Recon Shoulder `armour="shoulder"	y="4"	x="10"	angle="-5" flipY="true"	health="200"	bullet="0.5"	cut="100"	blunt="0.5"`  
  
* **angle**  
  Equip angle offset in degrees. Used in case it makes more sense for the item to spawn upright, but equip sideways (e.g. arms.)  
  _Usage_: Number (in degrees)  
  _Example_: `angle="-5"` to make Recon Shoulder equip slightly tilted without having to rotate the model.  
  
* **cutProtect**  
  Is the armour piece protective against cuts?  
  _Usage_: true/false  
  _Example_: `cutProtect="false"` cuts will pass through the armour piece to the bodypart.  
  
* **hideHead**  
  Hides the doll's head when the armour piece is equipped.  
  _Usage_: true (default is false and not required.)    
  _Example_: `hideHead="true"` can be used for head replacements (e.g. Caved Head)  
  
* **allowOther**  
  Other armour pieces can be equipped in addition to the current one.  
  _Usage_: true (default is false and not required.)  
  _Example_: `allowOther="true"` allows hats to be placed on Suppressor Head.  
  
* **collideDolls**  
  Makes the armour piece collide with dolls, including the one it equips on. May cause collision issues, but is required for armour pieces that are also meant to be used as weapons.  
  _Usage_: true (default is false and not required.)  
  _Example_: `collideDolls="true"` to make Gladiator Scissors be able to hit dolls.   
  
* **linkOnEquip**  
  Links the whole doll to the item when equipped.  
  _Usage_: true (default is false and not required.)  
  _Example_: `linkOnEquip="true"` makes Shielding Eye link to the doll, making its trigger functionality ignore the doll and not push it around.  
  See XML below.  
  
* **triggerOnEquip**  
  Automatically triggers when equipped.  
  _Usage_: true (default is false and not required)  
  _Example_: Shielding Eye is not equipped by default, but triggers on equip, activating the bubble shield.  
  `trigger="triggers::Bubbleshield" triggerRadius="50" triggerForce="1000" linkOnEquip="true" triggerOnEquip="true" triggered="false"`  
  
