# Properties

List of properties available to be used in functionality XML.

Can be used with:  
[General](https://gitlab.com/RavaGames/madlab/-/blob/master/Attributes/General.md) (e.g. `properties="sharp"`)  
and  
[Firearm](https://gitlab.com/RavaGames/madlab/-/blob/master/Attributes/Firearm.md) (e.g. `bulletProperties="poisoning"`)

This list is auto-generated. Some properties may be unused, or not work when combined with others. Experiment.

_Usage_: Comma-separated case-sensitive property names.  
_Example_: `properties="sticky,radioactive"` to make a dirty, dirty, item.

----

alchemying

alive

animated

antigrav

banning

blighting

blunt

breakable

brittle

bulletproof

burning

cauterizing

cleaning

concussing

critting

crumble

deleting

disintegrating

disintegrator

dispelling

draining

drying

explosive

fixedrotation

flaming

flammable

freezing

frostfiring

frying

ghost

glassing

glitching

gusting

healing

hyping

implosive

invisible

janitoring

killing

magnet

magnetic

mute

oily

petrified

petrifying

poisoning

projectileGhost

radioactive

regenerating

relaying

rending

resistAdrenaline

resistAliveBreak

resistArc

resistBanning

resistBleed

resistBlighting

resistChilling

resistConcuss

resistCutting

resistDisintegrating

resistDispel

resistDrain

resistDroning

resistDrying

resistFlame

resistFreeze

resistFrostfiring

resistFrying

resistGassy

resistGhosting

resistGlassing

resistGlitching

resistGravity

resistGust

resistHeal

resistKill

resistMagnetic

resistOily

resistPetrifying

resistPoison

resistProtect

resistRadioactive

resistScorching

resistShock

resistSnare

resistStasis

resistTransmuting

resistUltrasharp

resistWarming

resistWebbing

sanguine

saveStains

scorching

sensor

sharp

shocking

spreading

stainless

sticky

suspending

transmuting

triggerOnCut

triggering

ultrasharp

unbreakable

unequipConcuss

unequipDead

vanilla

wallGhost

warming

webbing

wetting

zerograv

