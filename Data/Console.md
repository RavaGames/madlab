# Console Commands

This is a list of commands usable within the ingame console, which you can access through the **Home** key, or through **Settings >  Menus > Console**. It may be useless to most people but me - most of them are commands useful for debugging.

_Usage_: Type command into console with required arguments.

----

**close**  
Closes the console.  
  
**contracts**  
Opens legacy Contracts menu.  
  
**credits**  
Displays development tools and credits.  
  
**detaillocked** [0/1] (default: 1)
Temporarily enables or disables Detail tool area selecting locked items.

**disablepause** [0/1] (default: 0)  
Entirely disables pause/unpause functionality so you can't accidentally unpause while building things.  
Resets on game restart.  
(alternatively **PleaseStopMeFromPressingAllTheseButtonsThankYou**)  
  
**environment** [Environment's Unique Name]  
Changes environment. (e.g. "environment moon")  
  
**fps**  
Toggles fps counter in upper left corner.  
  
**fullscreen**  
Toggles fullscreen.  
  
**howmanyitems**  
Counts the amount of items in the Library.  
  
**id** / **whatis** [Item's Library Name]  
Displays the internal unique id of the item.  
e.g. "id Speaker Drone" = dronespeaker  
  
**integerscaling** / **snap** [0/1] (default: 0)  
Sets whether or not item positions snap to the nearest full pixel when moved while paused.  
  
**itemsbackup**  
Restores Custom Items from the backup file.  
Do not do this unless everything is broken, as it will overwrite your custom items.  
  
**library**  
Forces Library to refresh, loading all local content and rebuilding item lists.  
  
**modelimport**  
**modelmanage**  
Methods for accessing legacy item models.  
Use [Library > Item Models] instead.  
  
**pause**  
**unpause**  
Predictably, pauses / unpauses.  
  
**prioritizeselect** / **ps** [0/1] (default: 1)  
Toggles whether tools prioritize selected items.  
Resets on game restart.  
  
**reset**  
Resets. Same as pressing Esc.  
  
**resetcustoms**  
**resetfavs**  
**resetmenus**  
**resetpowers**  
**resetsettings**  
Resets various things, same as reseting them via settings but available as a command in case the UI is broken.  
  
**selectcolour** [Colour RRGGBB] (default: EEEEEE)  
Temporarily changes select glow colour (e.g. "selectcolour FF0000" to make it red.)  
  
**spawn**  
Arguments: [Item Unique Name] [x position] [y position] [angle in degrees]  
Spawns an item. (e.g. "spawn ragdoll 100 100 45")  
For position upper left corner is (0, 0).  
  
**spawnCustom** [0-9999]  
Spawns a custom item in the order seen in the Library list, starting with 0.  
  
**stampquality** [0/1] (default: 1)  
Toggles whether or not stamps are anti-aliased (default) or crispy.  
Resets when the game is restarted.  
  
**togglemenus**  
Toggles all menu visibilities.  
Used for checking for pesky invisible menus blocking clicks.  

**whatdoesthisdo**  
Provides functionality information for the item currently selected with the detail tool, in an XML format.  
Useful for MaD Lab functionality things.  
  
**workshoplogs** (Steam)
Displays debug logs used to diagnose Steam Workshop issues.

**xml** [Item Library Name]  
Displays the library data for the item, in an XML format.  
e.g. "xml Speaker Drone" = *\<node label="Speaker Drone" object="dronespeaker" lab="true" tooltip="Babbles tips and stuff."/>*  
  
**yarr**  
Yarr at mouse.
