# Projectiles

Available projectiles for [Firearm](https://gitlab.com/RavaGames/madlab/-/blob/master/Attributes/Firearm.md) functionality XML. Projectile names are case-sensitive.

This list is auto-generated. Some projectiles may be unused, or not work at all. Experiment.

_Usage_: Projectile name.  
_Example_: `projectile="arrow"` to make a bow.

----
AA

aciddrop

acidpotion

adrenaline

aimarrow

airbomb

ammoniaBullet

aprocket

arrow

assault

autogrenade

back

ballista

bane

banish

bass

bb

bea

beam

bee

bfg

blast

blaster

bleed

bling

blowdart

bomb

boulder

breach

bubble

cactusspike

can

candycane

car

carBig

cluster

coffee

concuss

confetti

cookie

cookier

corrosiveray

crit

crossbow

crumbler

crush

cryobullet

cupid

cursor

death

deathdart

decoration

deepfry

dis

drainbullet

drone

droner

electro

elvenarrow

error

explosivedisc

extinguish

fat

firework

flamer

flare

fli

flm

food

foodray

frost

frostfire

furniture

futuristic

gas

ghost

ghostkiller

gib

glass

glitch

glue

grapple

grapplegun

gravity

gro

growingtomato

guillotine

hail

harpoonbullet

heals

heavypotato

ice

icicle

icicle2

implosive

ketchup

las

las2

launcher

lava

lemon

levity

light

longarrow

magma

magnet

maimarr

micro

musk

muzzleflash

nail

nestarrow

net

neurogas

noisybullet

paintdrop

panzer

petrify

pizza

plunger

poisonbullet

portal

potato

powder

propertybullet

protect

punk

quake

randomBlunt

randomExplosive

randomGun

randomSword

repulsor

ricochet

rifle

riflegrenade

riot

ripper

rpg

sandstone

sausage

saw

scorpionspike

shake

shakePower

shr

shrimp

sleep

smg

smoke

snarebullet

sni

snowball

snowman

soularr

spiderweb

splatter

stasis

stasisbullet

steam

steamarrow

steelarrow

tank

targetarrow

targeticicle

taserbullet

teleport

tidal

tinyele

tinymissile

toxinbullet

tranq

transmute

trapdart

triggerbullet

trirocket

twisterbullet

voidbullet

waterdrop

web

windbullet

worm

zap
