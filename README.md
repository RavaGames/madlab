[![](./img/MaDLabBanner.png)](https://rava.games/madlab/)

## MaD Lab

[MaD Lab](https://rava.games/madlab/) is the item editor made from scratch specifically for [MaD2](https://rava.games/mutilate/) content creation.

At its core MaD Lab is a relatively simple polygon editor that creates an item model file that contains everything necessary for the game to generate both the graphics and the colliders for items.

It has been used to design almost all models in the game (excluding some MaD1 items, and projectiles.)

And recently, item functionality as well.

## Functionality System

[The Cryolab update](https://steamcommunity.com/games/665370/announcements/detail/2906476183756739210)<sup>(steamcommunity.com)</sup> added an XML-based model functionality support to MaD Lab and MaD2, which has since been expanded to be the main content creation workflow for the game. All item models with functionality have used this system since then.

The system was originally made to streamline the item creation workflow during development, by moving into a data-oriented system where each model inherently contains everything necessary to allow for quick editing and tweaking. But as a happy side-effect the system also made it possible for players to attach existing functionality to their models and tweak their values to suit their needs.

This documentation does its very best to get you started with adding functionality to items, but as it is primarily a dev tool, it will likely be confusing, and some things you would want to do probably aren't possible because I didn't think of them at the time. [Ask](/CONTRIBUTING.md) if you need help or have any ideas for improvements.

## Getting Started

1. [Download the latest version of MaD Lab](https://rava.games/madlab/)<sup>(rava.games)</sup> for full XML functionality support.

2. Design a new item model and save it (producing _Item-Name_MaDLab.png_ model file), or use an existing one [submitted](/CONTRIBUTING.md) by others.

3. Check out an [XML Tutorial](https://www.w3schools.com/xml/xml_whatis.asp)<sup>(w3schools.com)</sup> if you're not familiar with the syntax, as malformed XML will fail to load in-game.

4. Consider using a text editor e.g. [Notepad++](https://notepad-plus-plus.org/)<sup>(notepad-plus-plus.org)</sup> for XML syntax highlighting and better text editor features in general.

5. See [Examples](/EXAMPLES.md) to get started. Copypaste an example into your model's Functionality field in MaD Lab.

6. Test the model by launching the game, opening the models folder via [Library > Item Models > Open Folder], and placing the model file there.

7. Use [Library > Item Models > Refresh] whenever changes to the model are made and they will be available in-game immediately.<br>Note that previous saves won't be updated with the new model values.

8. Tweak the values present in the example (e.g. force/radius), or add new supported values via the [full documentation](#attributes), and overwrite the model file.<br>Remember to refresh whenever making changes. No need to restart the game.

9. Make cool things.

10. [Submit the model](/CONTRIBUTING.md) for review and it might just end up in the game officially, or simply use it for your own nefarious purposes.

----

## Attributes

[General](/Attributes/General.md)

[Trigger](/Attributes/Trigger.md)

[Firearm](/Attributes/Firearm.md)

[Armour](/Attributes/Armour.md)

## Data

[Properties](/Data/Properties.md)

[Projectiles](/Data/Projectiles.md)

[Triggers](/Data/Triggers.md)
